"""Author: Francesco Bronzino
Description: Module that implements an alto server locally with a preloded database of addresses
"""

import math
import sys

DEBUG = True

def toRad(value):
	"""Converts numeric degrees to radians."""
	return value*math.pi/180;

class AltoClient:
	"""Simulate the behaviour of the ALTO server in a dummy way. Needs a preloded database.
	If the address requested is not in the db it will use a default distance.
	"""
	def __init__(self, cFile, DEFDIST = 10000000000):
		"""Arguments:
		cFile -- name of the file used to load the addresses DB
		DEFDIST -- the default distance used in case a requested address is not in the DB
		"""
		self.data = None
		self.altoSimulation = self.dummyOrder
		self.cFile = cFile
		self.setCords()
		self.queries = {}
		self.DEFDIST = DEFDIST
		
	def setCords(self):
		"""Parse the file that contain the hostnames and the coordinates."""
		f = open(self.cFile,'r')
		nodes = []
		for line in f:
			nodes.append(line.split(','))
		f.close()
		self.coords = {}
		for node in nodes:
			node[0] = node[1].rsplit()[0]
			try:
				self.coords[node[0]] = [float(node[2].rsplit()[0]),float(node[3].rsplit()[0])]
				if DEBUG: print "ALTO: new address added:", node[0], self.coords[node[0]][0],self.coords[node[0]][1]
			except:
				if DEBUG: print "ALTO: ERROR while reading address", node
		
	def distance(self, nodeA, nodeB):
		"""Calculate the distance in km between two nodes.
		
		Arguments:
		nodeA -- node A coordinates (array of 2 elements)
		nodeB -- node B coordinates (array of 2 elements)
		
		Return:
		the distance between node A and node B
		"""
		R = 6371; #Radius of the earth in km
		dLat = toRad(nodeB[1]-nodeA[1])
		dLon = toRad(nodeB[0]-nodeA[0]) 
		a = math.sin(dLat/2)*math.sin(dLat/2)+math.cos(toRad(nodeA[1]))*math.cos(toRad(nodeB[1]))*math.sin(dLon/2)*math.sin(dLon/2); 
		c = 2*math.atan2(math.sqrt(a), math.sqrt(1-a)); 
		d = R*c;
		return d
		
	def dummyOrder(self, clientAddr, otherAddrs, distance):
		try:
			dists = []
			for addr in otherAddrs:
				if self.coords.has_key(addr) and self.coords.has_key(clientAddr) and addr != clientAddr:
					dists.append((addr, self.distance(self.coords[clientAddr], self.coords[addr])))
				else:
					dists.append((addr, self.DEFDIST))
		except Exception:
			if DEBUG: print 'ALTO ERROR: no address', addr,clientAddr,`sys.exc_info()`
		try:
			ret = []
			for d in dists:
				if d[1] <= distance:
					ret.append(d)
			return ret
		except Exception:
			if DEBUG: print 'ALTO ERROR: error while ordering or returning',`sys.exc_info()`
		
		
		
	def analyze(self, clientAddr, otherAddrs, distance):
		ret = self.altoSimulation(clientAddr, otherAddrs, distance)
		return ret
		
	def getCoords(self, node):
		return self.coords[node][0],self.coords[node][1]
