"""
	Module used to implement the stats logger
"""

from cdn.utils import *

class StatsLogger:
	"""
		Class that implement a logger for logging stats
	"""
	def __init__(self, filename):
		"""
			* filename: name of the log file
		"""
		self.filename = filename
		
	def startLogging(self,time):
		"""
			Open the file used for logging
		"""
		self.log = open(self.filename, 'w')
		self.log.write(INITT+','+str(time)+'\n')
		
	def finishLogging(self, time):
		"""
			Close (and flush) the file used for logging
		"""
		self.log.write(FL+','+str(time)+'\n')
		self.log.close()
		
	def log(self, msg):
		"""
			Log a message as it is
		"""
		self.log.write(msg+'\n')
		
	def logType(self, t, time):
		"""
			Log a a time with the correspodent tag
		"""
		self.log.write(t+','+str(time)+'\n')
		
	def logRequestTime(self,request_time, time):
		"""
			Store the time needed to the user to download the entire content
			request_time: time at wich the request has been generated
			time: time requeired to retrive it 
		"""
		self.log.write(RTIME+','+str(request_time)+','+str(time)+'\n')
		
	def logFirstTime(self,request_time, time):
		"""
			Store the time needed to have the first chunk available
			request_time: time at wich the request has been generated
			time: time requeired to retrive it 
		"""
		self.log.write(FTIME+','+str(request_time)+','+str(time)+'\n')
		
	def logNContents(self, time, n):
		"""
			Store the number of contents
			time: time of log
			n: number of contents in the cep cache
		"""
		self.log.write(NCONTS+','+str(time)+','+str(n)+'\n')
		
	def logHTTPConn(self,time):
		"""
			Store the time at wich an HTTP connection has been made
			time: time of the connection
		"""
		self.log.write(HTTPC+','+str(time)+'\n')
		
	def logP2PConn(self,time):
		"""
			Store the at wich an P2P connection has been made
			time: time of the connection
		"""
		self.log.write(P2PC+','+str(time)+'\n')
		
	def logCacheConn(self,time):
		"""
			Store the at wich an P2P connection has been made
			time: time of the connection
		"""
		self.log.write(CACHEC+','+str(time)+'\n')
		
	def logCDNFirstCDownload(self,total_time):
		"""
			Total time to download a content from a cdn node
		"""
		self.log.write(CDNFCT+','+str(total_time)+'\n')
		
	def logP2PFirstCDownload(self,total_time):
		"""
			Total time to download a content using p2p
		"""
		self.log.write(P2PFCT+','+str(total_time)+'\n')
		
	def logCacheFirstCDownload(self,total_time):
		"""
			Total time to download a content using p2p
		"""
		self.log.write(CACHEFCT+','+str(total_time)+'\n')
	
	def logCDNDownload(self,total_time):
		"""
			Total time to download a content from a cdn node
		"""
		self.log.write(CDNT+','+str(total_time)+'\n')
		
	def logP2PDownload(self,total_time):
		"""
			Total time to download a content using p2p
		"""
		self.log.write(P2PT+','+str(total_time)+'\n')
		
	def logDownload(self, time):
		"""
			Download started at time time
		"""
		self.log.write(DT+','+str(time)+'\n')
		
	def logCDNChunk(self,time):
		"""
			Download of a chunk from a CDN node started at time time
		"""
		self.log.write(CDNC+','+str(time)+'\n')
		
	def logCEPChunk(self,time):
		"""
			Download of a chunk from a CDN node started at time time
		"""
		self.log.write(CEPC+','+str(time)+'\n')
