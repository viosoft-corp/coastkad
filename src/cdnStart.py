"""Author: Francesco Bronzino
Description: Module used to start an instance of a CDN node
"""

#!/usr/bin/env python

from twisted.internet import reactor
from cdn.webServer import CdnHTTPServer
from cdn.conf import config as cdnConf
from cdn.db import DB
from cdn.statsLogger import StatsLogger
from cdn.webServer import CdnHandler
import os
import time

DEBUG = True

DBFILE = cdnConf.getstring('DEFAULT', 'DBFILE')
WSPORT = cdnConf.getint('DEFAULT', 'WSPORT')
WSPATH = cdnConf.getstring('DEFAULT', 'WSPATH')
WSFILE = cdnConf.getstring('DEFAULT', 'WSFILE')
CSIZE = cdnConf.getint('DEFAULT', 'CSIZE')
LOGGER = cdnConf.getstring('DEFAULT', 'LOGGER')


class CdnBuilder:
	"""Class used to instantiate a CDN object."""
	
	def __init__(self):
		"""The parameters used are taken from the configuration file."""
		try:
			os.stat(WSPATH)
		except:
			os.makedirs(WSPATH)
		self.db = DB(DBFILE)
		self.logger = StatsLogger(LOGGER)
		self.logger.startLogging(time.time())
		self.wserver = CdnHTTPServer(('', WSPORT), CdnHandler)
		self.wserver.setup(WSPATH, None, self.logger, WSFILE, CSIZE)
		self.wserver.serve_forever()
		
	def start(self):
		"""Start the CDN object job."""
		reactor.run()
		self.logger.finishLogging(time.time()-self.initTime)
		
	def stop(self):
		reactor.stop()
		
if __name__== "__main__":
	c = CdnBuilder()
	c.start()
