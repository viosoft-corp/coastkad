"""Author: Francesco Bronzino
Description: Module that implements the cache of a CEP node.
"""

#TODO: inserire la dht affinche' si possano impostare i timeout per mantenere i valori aggiornati. Per il momento non e' necessario perche' usiamo infinite cache

import sys
from db import DB
from cep.utils import *

LRU = 1
LFU = 2

DEBUG = True

class CepCache:
	"""
	An implementation of the cache of a CEP.
	The class uses two elements to form the cache: a database where the information about the status of the cache is stored and an interface
	the filesystem to store or remove the files in to/from the cache.
	"""
	def __init__(self, db, size, policy, filesystem):
		"""Arguments:
		db -- the filename of the DB
		size -- size of the db in ??? (only tested with infinite cache)
		policy -- value representing a caching policy (LRU, LFU)
		filesystem -- object used to organize connect the cache with the filesystem
		"""
		self.size = size
		self.used = 0
		self.policy = policy
		if self.policy == LRU:
			self.insert = self.insertLRU
			self.getByKey = self.getByKeyLRU
			self.updateValue = self.updateLRU
		elif self.policy == LFU:
			self.insert = self.insertLFU
		self.cacheDb = DB(db)
		self.cacheMemory = filesystem
		
	def hasKey(self, key):
		"""Check if there is the object corresponding to the key.
		
		Arguments:
		key -- the key to check for
		"""
		#Per il momento non ci sono altre funzioni per capire se ha la chiave o meno se non recuperare l'intero valore
		#return self.cache.countValues(key)>0
		return self.cacheDb.retrieveValue(key) != None
		
	def updateSize(self,key,size):
		"""Update the size of a content cached.
		
		Arguments:
		key -- the key of the object
		size -- the new size value
		"""
		self.cacheDb.updateSize(key,size)
		
	def updateLRU(self, key):
		"""Just update the timestamp related to the key.
		
		Arguments:
		key -- object's key
		"""
		self.cacheDb.updateTime(key)
		
	def getByKeyLRU(self, key):
		"""Return a content if the key is in the cache
		
		Arguments:
		key -- object's key
		
		Return:
		an array [size of the content, path in the system, bitmap]
		"""
		#Update the timestamp of the objact
		self.cacheDb.updateTime(key)
		#Per il momento ritorna size,path,map
		temp = self.cacheDb.retrieveValue(key)
		ret = None
		if temp:
			ret = {}
			ret['size'] = temp[0]
			ret['path'] = temp[1]
			ret['map'] = Bitmap(m = temp[2])
		return ret
		
	def insertLFU(self, key, value, path, m):
		"""Insert or update a key value pair using Least Frequently Used method.
		***Still not implemented***
		"""
		print 'insert1'
		
	def insertLRU(self, key, size, filename, m, buf):
		"""Insert or update a key value pair using Least Recently Used method.
		
		Arguments:
		key -- the key of the value to insert
		size -- the size of the file expressed in number of chunks(ok if we use a constant as the size of a chunk
		path -- the filename to use storing the value in the filesystem
		m -- the bitmap to express the chunks owned
		buf -- array with buffer objects that contain the chunk
		"""
		#Non considerato il caso in cui un oggetto sia piu' grande dell'intera cache...
		path = filename
		if self.size <= 0:
			if DEBUG: print 'CepCache: Infinite cache: adding new element'
			self.cacheDb.storeValue(key,size, path, m.getMap())
			if DEBUG: print 'CepCache: inserito in cache'
			self.cacheMemory.storeByMap(filename, buf, m)
			if DEBUG: print 'CepCache: inserito in memoria'
		elif self.size - self.used - size >= 0:
			if DEBUG: print 'CepCache: Space available: adding new element'
			self.cacheDb.storeValue(key, size, path, m)
			if DEBUG: print 'CepCache: inserito in cache'
			self.used += size
			self.cacheMemory.storeByMap(filename, buf, m)
			if DEBUG: print 'CepCache: inserito in memoria'
		else:
			if DEBUG: print 'CepCache: Cache full: adding new element'
			while not self.size - self.used - size >= 0:
				k = self.cacheDb.LRU()
				if DEBUG: print 'CepCache: eliminating value', `k`
				#size,path,map
				l = self.cacheDb.deleteValue(k)
				mTemp = bitmap(l[2])
				self.cacheMemory.deleteByMap(l[1],mTemp)
				self.used -= l[0]
				if DEBUG: print 'CepCache: Used cache:',self.used 
			self.cacheDb.storeValue(key, size, filename, m)
			if DEBUG: print 'CepCache: inserito in cache'
			self.cacheMemory.storeByMap(filename, buf, m)
			if DEBUG: print 'CepCache: inserito in memoria'
			self.used += size
			
	def updateMap(self, key, filename, m, buf):
		"""Update the bit map of the chached element.
		
		Arguments:
		key -- the key of the value to update
		filename -- the filename to update
		m -- the bitmap to express the chunks owned
		buf -- array with buffer objects that contain the chunk
		"""
		old = self.cacheDb.updateMap(key,m.getMap())
		diff = m.diff(old)
		self.cacheMemory.storeByChunk(filename, buf, diff)
		
	def lockValue(self,key):
		"""Lock a cached element that will not be deleted while been locked.
		
		Arguments:
		key -- object's key
		"""
		self.cacheDb.lockValue(key)
		
	def unlockValue(self, key):
		"""Unlock a cached element that will available to be deleted.
		
		Arguments:
		key -- object's key
		"""
		self.cacheDb.unlockValue(key)
			
	def deleteValue(self, key):
		"""This function is used to delete a value from the cache.
		
		Arguments:
		key -- object's key
		"""
		#In teoria questa funzione non serve perche' la cache si libera nel momento in cui e' piena e devo
		#fare spazio. In ogni caso e' utile averla
		l = self.cacheDb.deleteValue(k)
		#size,path,map
		m = bitmap(l[2])
		self.cacheMemory.deleteByMap(l[1],m)
		self.used -= l[0]
	
	def clearCache(self):
		"""Delete all the elements that are contained in the cache."""
		self.cacheDb.clearCache()
		#Rimuovere dalla cartella cache tutti i valori
		
	def getNContents(self):
		"""Return the number of contents in cache.
		
		Return:
		the number of contents
		"""
		try:
			return self.cacheDb.countValues()
		except Exception:
			return 0
		
class testCepCache:
#Da riadattare al fatto che ora c'e' la memoria
	def __init__(self, size, policy):
		self.cache = CepCache(size, policy)
		
	def runTest(self):
		self.cache.insert('key', 1, '/path/prova',1)
		self.cache.insert('key2',1, '/path/prova2',1)
		self.cache.insert('key3',1, '/path/prova3',1)
		self.cache.insert('key4',1, '/path/prova4',1)
		self.cache.insert('key5',1, '/path/prova5',1)
		self.cache.insert('key6',1, '/path/prova6',1)
		self.cache.insert('key7',1, '/path/prova7',1)
		print self.cache.getByKey('key')
		print self.cache.getByKey('key2')
		print self.cache.getByKey('key3')
		print self.cache.getByKey('key4')
		print self.cache.getByKey('key5')
		print self.cache.getByKey('key6')
		print self.cache.getByKey('key7')
		self.cache.clearCache()
		
if __name__=='__main__':
#Run a complete test on the cache manager
	t = testCepCache(int(sys.argv[1]),int(sys.argv[2]))
	t.runTest()
