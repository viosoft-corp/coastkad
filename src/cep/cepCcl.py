"""Author: Francesco Bronzino
Description: Module that implements the cep Content Cache Locator
"""

#TODO: implementare adattabilita' a diverse dimensioni dei contenuti

#IDEA TODO: testare il caso in cui in ogni caso attivo il p2p, per dimostrare che le prestazioni non rimangono sufficienti.

from twisted.internet.defer import Deferred
from twisted.internet import reactor
from cep.dhtInterface import DhtInterface
from cep.alto import AltoClient
from cep.utils import *
import sys
import random

DEBUG = True

class Ccl:
	"""Decision logic that decides if to retrieve a content using a normal HTTP_GET or by a P2P download.
	"""
	def __init__(self, address, cdn, altoClient, dht = None, nodes = None, MINP2P = 2, MAXCONC = 10, P2PTH = 30, ACTP2P = True, CSIZE = 1600, P2PQ = 0.75, MAXP2P = 20):
		"""Arguments:
		address -- the address of the node
		cdn -- a list of CDN nodes: [((host,port),distance),...]
		altoClient -- ALTO Client to send ordering queries
		dht -- the DHT client for DHT queries
		nodes -- DEPRECATED
		MINP2P -- minimum number of nodes to start P2P (see config file)
		MAXCONC -- maximum number of peers to use for P2P
		ACTP2P -- boolean, if true P2P is usable
		CSIZE -- size of contents (same size for all contents)
		P2PQ -- threshold to use in the last quartile for evaluating if using P2P
		MAXP2P -- minimum number of nodes in which case P2P is activated
		"""
		self.IDht = dht
		self.altoClient = altoClient
		self.waitingClients = {}
		self.dhtValues = {}
		self.address = address
		self.MINP2P = MINP2P
		self.MAXCONC = MAXCONC
		self.P2PTH = P2PTH
		self.ACTP2P = ACTP2P
		self.CSIZE = CSIZE
		self.cdn = cdn
		self.P2PQ = P2PQ
		self.MAXP2P = MAXP2P
		
	def sendBackCDN(self, key):
		"""If download from CDN is selected this function prepare the list of CDN nodes to use for download.
		
		Arguments:
		key -- object's key
		"""
		for v in self.waitingClients[key]:
			try:
				v.callback((key,HTTP,self.cdn, 0))
			except Exception:
				if DEBUG: print 'CepCCL ERROR: send back cdn: ',`sys.exc_info()`
		del self.waitingClients[key]
		if self.dhtValues.has_key(key):
			del self.dhtValues[key]
			
	def sendBackP2P(self, key, dim):
		"""If download with P2P is selected this function prepare the list of CEP-CDN nodes to use for download.
		
		Arguments:
		key -- object's key
		"""
		rnd = random.randint(0,len(self.cdn)-1)
		values = self.dhtValues[key]
		values.append({'address':self.cdn[rnd][0],'flag': Bitmap(s = dim)})
		for v in self.waitingClients[key]:
			try:
				v.callback((key,P2P,values, dim))
			except Exception:
				if DEBUG: print 'CepCCL ERROR: send back p2p: ',`sys.exc_info()`
		del self.waitingClients[key]
		del self.dhtValues[key]
		
	def thEval(self,key):
		"""Function that calculate a value corresponding to the availability of the requested content based on the known peers.
		
		Arguments:
		key -- object's key
		"""
		n = len(self.dhtValues[key])
		p = int(n*self.P2PQ)
		#Prendo l'ultimo quartile dei valori ordinati crescentemente in base al totale di chunk posseduti
		srtd = sorted(self.dhtValues[key], key= lambda v: v['flag'].getTot())[p:]
		perc = 0
		for v in srtd:
			try:
				perc += v['flag'].getPerc()
			except Exception:
				continue
		perc = float(perc)/len(srtd)
		return perc
		
	def actP2P(self,key):
		"""return true if P2P usable, false otherwise.
		
		Arguments:
		key -- object's key
		"""
		th = self.thEval(key)
		#print th
		if th > self.P2PTH:
			return True
		else:
			return False
		
	def decisionLogic(self, (key,dim)):
		"""Based on the information received from the dht and ordered by ALTO, the ccl decide which tecnology to use for the content delivery.
		
		Arguments:
		key -- object's key
		dim -- content's size
		"""
		if DEBUG: print 'CepCCL: decision logic for key',key
		act = False
		try:
			act = self.actP2P(key)
		except Exception:
			if DEBUG: print 'CepCCL ERROR: calculating perc: ',`sys.exc_info()`
			self.sendBackCDN(key)
		if act:
			if DEBUG: print 'CepCCL: P2P is activatable'
			self.sendBackP2P(key, dim)
		else:
			if DEBUG: print 'CepCCL: not enough contents in peers'
			self.sendBackCDN(key)
		
	def gotReplyFromDht(self, (key, values)):
		"""Once received the values requested to the dht, the ccl decide if contact ALTO or start download from the cdn.
		
		Arguments:
		key -- object's key
		values -- list of values returned by the DHT query
		"""
		try:
			#Start the decision process based on the values returned from the dht
			#Posso decidere delle politiche secondo cui non contatto alto e passo direttamente la risposta
			#Per esempio: se ho un solo indirizzo ritornato dalla dht o simili
			#Values e' un array di stringhe le quali rappresentano i flag desiderati
			if DEBUG: print 'CepCCL: Received reply from DHT',key
			#Aggiungere il fatto che potrebbero esserci meno che il minimo indispensabile di nodi per iniziare il p2p
			if not len(values) or (len(values) < self.MINP2P):
				if DEBUG: print 'CepCCL: No values from the DHT'
				#Questo e' il caso in cui la dht non contiene il valore richiesto
				self.sendBackCDN(key)
				return 
			#Salvo i valori che mi ha restituito la dht
			if DEBUG: print 'CepCCL: Received valus from DHT',values
			self.dhtValues[key] = values
			#Richiedo ad ALTO di riordinarmi gli indirizzi
			addrs = []
			for v in values:
				addrs.append(v['address'][0])
			df = self.altoClient.analyze(key, self.address[0], addrs)
			df.addCallback(self.gotReplyFromAlto)
		except Exception:
			if DEBUG: print 'CepCCL ERROR in function gotReplyFromDht:',`sys.exc_info()`
		
	def errorDHT(self,key):
		"""Error while retrieveing the value corresponding to key from the DHT.
		
		Arguments:
		key -- object's key
		"""
		if DEBUG: print 'CepCCL ERROR: error contacting the dht for key',key
		for v in self.waitingClients[key]:
			p = self.waitingClients[key].pop(0)
			reactor.callFromThread(p.errback,key)
		del self.waitingClients[key]
	
	def seeder(self,key):
		"""Check if there is at least one seeder in the peers list.
		
		Arguments:
		key -- object's key
		"""
		for v in self.dhtValues[key]:
			s,i,t = divFlag(v['flag'])
			if s:
				return t
		return 0
		
	def gotReplyFromAlto(self, (key, addrs)):
		"""Received the values reordered by alto.
		
		Arguments:
		key -- object's key
		addrs -- list of ordered addresses
		"""
		try:
			#L'intera funzione andrebbe ottimizzata
			#Una volta ricevuta la lista degli indirizzi riordinata da alto si passa alla decision logic
			if DEBUG: print 'CepCCL: Got reply from ALTO',key
			#seleziono solo un massimo di MAXCONC valori che siano piu' vicini della cdn
			for i in range(len(addrs)):
				if self.cdn[0][1] <= addrs[i][1] or i == self.MAXCONC:
					addrs = addrs[:i]
					break
			#Se non ho un numero sufficiente di cep vicini con cui fare p2p vado dalla dht
			if len(addrs) < self.MINP2P:
				if DEBUG: print 'CepCCL: Not enough peers available to start P2P', len(addrs)
				self.sendBackCDN(key)
				return
			temp = []
			#Prendo dalla lista iniziale di valori quelli che mi interessano
			#Questo sarebbe da ottimizzare un attimo
			for addr in addrs:
				for value in self.dhtValues[key]:
					if value['address'][0] == addr[0]:
						temp.append(value)
						break
			self.dhtValues[key] = temp
			#Questo valore per il momento e' salvato a prescindere ma c'e' bisogno di essere certi di trovare una maniera di calcolarlo a un certo punto(io penso che sia un'informazione che dovrebbe essere fornita precedentemente...
			#Un'altra soluzione e' passarlo via http header durante il protocollo p2p, ma tutto questo servirebbe solo durante i test
			dim = self.CSIZE
			#Genero le bitmap dei vari nodi
			addrs = []
			temp = []
			for v in self.dhtValues[key]:
				if v['address'] in addrs:
					continue
				addrs.append(v['address'])
				try:
					s,init,tot = divFlag(v['flag'])
					#print s,init,tot
				except Exception:
					if DEBUG: print "CepCCL ERROR: valori per il flag sbagliati:",v['flag'],`sys.exc_info()`
					s = 0
					init = 0
					tot = 0
				v['flag'] = Bitmap()
				v['flag'].editFromPerc(init, tot, dim, self.MAXCONC)
				temp.append(v)
			self.dhtValues[key] = temp
			if DEBUG: print 'CepCCL: values that I can use for P2P:',self.dhtValues[key]
			#Se ho un numero sufficiente di nodi senza bisogno di andare a controllare quanto contenuto hanno
			#  attivo il p2p senza ulteriori controlli
			if len(addrs) >= self.MAXP2P:
				if DEBUG: print 'CepCCL: Enough peers available to start P2P', len(addrs)
				self.sendBackP2P(key, dim)
			else:
				#Non ho abbastanza peers per essere sicuro di attivare il P2P
				#Passo la parola all'ultima fase della decision logic
				reactor.callFromThread(self.decisionLogic, (key,dim))
		except Exception:
			if DEBUG: print 'CepCCL ERROR in function gotReplyFromAlto: ',`sys.exc_info()`
		
	def startLocation(self, key):
		"""Active the process of retrieving the value associated to a key and decide how to retrieve the content.
		It starts with the request to the DHT.
		
		Arguments:
		key -- object's key
		"""
		try:
			if DEBUG: print 'CepCCL: Start location for key',key
			#Genero il deferred con cui rispondero' al chiamante
			df = Deferred() 
			#Nel caso in cui ci sia gia' in corso una richiesta per la stessa chiave, aggiungo un altro deferred a cui ritornare il valore
			#Questo non dovrebbe accadere visto che cep e ccl risiedono sulla stessa macchina
			self.waitingClients.setdefault(key, []).append(df)
			if not self.ACTP2P:
				reactor.callFromThread(self.sendBackCDN, key)
				return df
			#Inoltro la richesta di valore alla dht
			df2 = self.IDht.getValue(key)
			df2.addCallback(self.gotReplyFromDht)
			df2.addErrback(self.errorDHT)
			return df
		except Exception:
			if DEBUG: print 'CepCCL ERROR in function startLocation:',`sys.exc_info()`
		
		
class test:
	def __init__(self):
		#from coastkad import kDht
		#from coastkad.kconf import config
		#self.dht = kDht.KDHT()
		#self.dht.loadConfig(config, config.get('DEFAULT', 'DHT'))
		self.alto = AltoClient('/home/wontoniii/coast/NodesCoordinates')
		#self.idht = DhtInterface(dht = self.dht)
		self.ccl = Ccl('133.1.172.60', [(('133.1.172.58',10011), 1000000000),], self.alto, dht = None, P2PTH = 1)
		df = Deferred()
		df.addCallback(self.print_res)
		self.ccl.waitingClients['1'] = [df,]
		
		
	def startTest(self):
		values = [{'flag': 2598132, 'address': ['152.3.138.7', 10111]}, {'flag': 1, 'address': ['193.137.173.217', 10111]}, {'flag': 1, 'address': ['155.185.55.185', 10111]}, {'flag': 1, 'address': ['150.244.58.159', 10111]}, {'flag': 1, 'address': ['128.83.122.152', 10111]}, {'flag': 1, 'address': ['155.207.48.52', 10111]}, {'flag': 1, 'address': ['128.112.139.18', 10111]}, {'flag': 1, 'address': ['128.83.122.152', 10111]}, {'flag': 1, 'address': ['155.207.48.52', 10111]}, {'flag': 1, 'address': ['150.244.58.159', 10111]}, {'flag': 1, 'address': ['128.83.122.152', 10111]}, {'flag': 1, 'address': ['155.207.48.52', 10111]}]
		reactor.callFromThread(self.ccl.gotReplyFromDht,('1',values))
		
	def print_res(self,(key, method, values, size)):
		print values
		
if __name__=='__main__':
	t = test()
	t.startTest()
	reactor.run()

