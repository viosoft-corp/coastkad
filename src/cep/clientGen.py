"""Author: Francesco Bronzino
Description: module that implement the schedule of the arrival of simulated clients to a cep node and execute them.
"""

from cep.cepInterface import CepInterface
from twisted.internet.defer import Deferred
from twisted.internet import reactor
import time

DEBUG = True

class CepClient:
	"""Represent a coast client; it requests a specific content to a cep node.
	"""
	def __init__(self,cep, key,logger):
		"""Arguments:
		cep -- CEP interface instance
		key -- object's key
		logger -- logger instance
		"""
		self.key= key
		self.logger = logger
		self.cep = cep
		
	def start(self):
		"""Send the retrieval request."""
		self.initR = time.time()
		if DEBUG: print 'CepClient: sending request at time:',self.initR
		df = self.cep.getContent(self.key)
		df.addCallback(self.allAvailable)
		
	def allAvailable(self,m):
		"""Informs that the content has been completely retrieved.
		
		Arguments:
		m -- UNUSED
		"""
		end = time.time()
		self.logger.logRequestTime(end - self.initR, end)
		if DEBUG: print 'CepClient: content completely available at time:',end

class ClientGenerator:
	"""Schedule the arrival of new clients."""
	def __init__(self, cep, arrivals, contents, logger, mreq = 0):
		"""Arguments:
		cep -- CEP interface to send the requests
		arrivals -- Random generator that return next arrival
		contents -- b of keys that return the next key representing the content to ask to the CEP
		logger -- logger instance
		mreq -- total time of simulation if needed
		"""
		#cep interface to send the requests
		self.cep = cep
		#Random generator that return next arrival
		self.arrivals = arrivals
		#Db of keys that return the next key representing the content to ask to the cep
		self.contents = contents
		#Logger used for statistics
		self.logger = logger
		#total time of simulation if needed
		self.mreq = mreq
		self.current = 0
		self.last = 0
		self.nreq = 0
	
	def schedule(self):
		"""Scheduler function."""
		if DEBUG: print 'ClientGenerator: scheduling next event'
		key = self.contents.getNext()
		time = self.arrivals.getNext()
		self.last = self.current
		self.current += time
		self.genRequest(key)
		self.nreq += 1
		if DEBUG: print 'ClientGenerator: request n.',self.nreq,'in',time,'sec.'
		#sys.stdout.flush()
		if self.mreq:
			if self.nreq < self.mreq:
				reactor.callLater(time, self.schedule)
		else:
			reactor.callLater(time, self.schedule)
		
	def start(self, initwait):
		"""Start the scheduling process.
		
		Argumetnts:
		initwait -- DEPRECATED
		"""
		time = self.arrivals.getNext()
		reactor.callLater(time,self.schedule)
		
	def genRequest(self, key):
		"""Given the object to request, starts the content request instantiating a new client object.
		
		Arguments:
		key -- object's key
		"""
		if DEBUG: print 'ClientGenerator: generating next client'
		c = CepClient(self.cep, key,self.logger)
		reactor.callLater(0,c.start)
