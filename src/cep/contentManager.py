"""Author: Francesco Bronzino
Description: module that implements basic classes to manage contents to use during simulations 
"""

import random
from cep.rgen import Zipf, Uniform, Normal48
from cep.dbClients import DB
from cep.utils import newID
import os,sys


class DBGenerator:
	"""Generate a database of keys to use in simulation runs."""
	def __init__(self, db):
		"""Arguments:
		db -- filename for the db
		"""
		self.db = DB(db)
		
	def gen(self, n):
		"""Generate contents keys.
		
		Arguments:
		n: number of contents to generate
		"""
		m = self.db.getMax()
		for i in range(n):
			self.db.storeValue(i+m+1,newID())
			
	def close(self):
		"""Closes the DB."""
		self.db.close()
			
class DBReader:
	"""Using a Zipf distribution read the keys from a database
	"""
	def __init__(self, db,slope,seed=None):
		"""Arguments:
		db -- filename for the db
		slope -- slope for the Zipf distribution
		seed -- seed
		"""
		self.db = DB(db)
		self.m = self.db.getMax()
		self.rand = Zipf((slope,self.m),x=seed)
		
	def getNext(self):
		"""Get next object's key using the random generator."""
		n = self.rand.randZRank()
		return self.db.retrieveValue(n)
		
if __name__ == "__main__":
	if sys.argv[1] == '1':
		g = DBGenerator(sys.argv[2])
		g.gen(int(sys.argv[3]))
		g.close()
	else:
		r = DBReader(sys.argv[2],1)
		#for i in range(int(sys.argv[3])):
		#	print r.getNext()
		print r.m
