"""
	Module that implements the interactions with the filesystem. Move the chunks buffered to the designed path
"""

#TODO: organizzare il mkdir affinche' crei la cartella se non esiste

import sys,os
from cep.buffer import Buffer
from cep.utils import createKey, divKey

DEBUG = False

class Filesystem:
	"""
		Class to interact with the filesystem
	"""
	def __init__(self, path):
		"""
			* path: path where the cached contents are stored. If None, no content will be ever stored in the filesystem
		"""
		self.path = path
		if path != None:	
			try:
				lf = os.stat(self.path)
			except:
				os.makedirs(self.path)
		else:
			if DEBUG: print 'Filesystem: i file non verranno salvati in memoria'
		
	def storeByChunk(self, filename, buf, chunk):
		"""
			Move the files stored in the buf array to the filesystem path designated to host the cache
		"""
		if DEBUG: print "Filesystem: storing chunk", filename, chunk
		buf.close()
		name = buf.getName()
		del buf
		if self.path == None:
			if DEBUG: print 'Filesystem: Elimino solamente il file temporaneo perche non voglio salvare i file'
			os.remove(name)
			return True
		try:
			print name
			os.rename(name, self.path+createKey(filename,chunk))
			if DEBUG: print 'Filesystem: Chunk moved to path',self.path
		except Exception:
			if DEBUG: print 'Filesystem ERROR: An error occurred while moving a chunk:'
			return False
		return True
		
	def deleteByChunk(self, filename, chunk):
		"""
			Delete all the files related to a filename
		"""
		if DEBUG: print "Filesystem: deleting chunk", filename, chunk
		if self.path == None:
			return True
		try:
			os.remove(self.path+createKey(filename,mr))
		except Exception:
			if DEBUG: print 'Filesystem ERROR: An error occurred while deleting a chunk:'
			
	def storeByMap(self, filename, buf, m):
		"""
			Move the files stored in the buf array to the filesystem path designated to host the cache
		"""
		if DEBUG: print "Filesystem: storing map", filename, m.getMap()
		ret = True
		mTemp = m.mapToArray()
		for i in mTemp:
			buf[i].close()
			name = buf[i].getName()
			if self.path == None:
				if DEBUG: print 'Filesystem: Elimino solamente il file temporaneo perche non voglio salvare i file'
				os.remove(name)
			else:
				try:
					os.rename(name, self.path+createKey(filename,i))
				except Exception:
					if DEBUG: print 'Filesystem ERROR: An error occurred while moving a chunk:'
					ret = False
		return ret
		
	def deleteByMap(self, filename, m):
		"""
			Delete all the files related to a filename
		"""
		if DEBUG: print "Filesystem: deleting map", filename, m.getMap()
		if self.path == None:
			return True
		ret = True
		mTemp = m.mapToArray()
		for i in mTemp:
			try:
				os.remove(self.path+createKey(filename,i))
			except Exception:
				if DEBUG: print 'Filesystem ERROR: An error occurred while deleting a chunk:'
				ret = False
		return ret
				
	
