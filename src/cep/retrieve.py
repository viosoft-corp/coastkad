"""
	Module that implements the classes used to retrieve a content.
	There are two different strategies to be utilized:
		-Retrieve from a list of cdn nodes
		-Retrieve from a list of other ceps using a dummy p2p protocol
"""

import sys
from twisted.internet import reactor
from twisted.internet.defer import Deferred
import cep.webClient
from cep.webClient import CepWebClient
from cep.utils import *
import random
import time

DEBUG = True

#TODO: capire come sia possibile che in alcuni casi lo stesso chunk tvenga chiesto a due nodi.

class RetrieveContent:
	"""
		General class that implements the basic classes to retrieve a content
	"""
	def __init__(self, httpClient, key, partCallback, partErrback, logger, par = 1, size = 1, tout = 60):
		"""
			* httpClient: client utilized to download via http-download
			* key: key that represent the content to retrieve
			* partCallback: function to call as soon as a chunk is retrieved
			* partErrback: function to call as soon as a chunk has not been retrieved and it will not be retrieved
			* par: number of parallel requestes permitted during he retrieval
			* size: number of chunks that form the content
		"""
		self.key = key
		#Bisognerebbe fare in modo che se la dimensione non e conosciuta la si calcola ricavandola dal primo chunk
		self.size = size
		#Array dimensione size i cui valori sono 1 se il chunk e' stato recuperato, 0 altrimenti
		self.retrievedChunks = []
		#Array dimensione size i cui valori sono 1 se si sta recuperando il chunk in questione, 0 altrimenti
		self.retrievingChunks = []
		for i in range(self.size):
			self.retrievingChunks.append(0)
			self.retrievedChunks.append(0)
		#Richieste in parallelo
		self.par = par
		#Funzione chiamata quando un chunk e stato recuperato
		self.partCallback = partCallback
		#Funzione chiamata quando un chunk non e' recuperabile
		self.partErrback = partErrback
		#Numero di chunks recuperati
		self.retrieved = 0
		#Numero di chunks che sto recuperando
		self.retrieving = 0
		#Finito
		self.finished = False
		#Client http da usare per i download
		self.httpClient = httpClient
		#Incompleto
		self.incomplete = False
		#logger
		self.logger = logger
		#Timeout time after wich drop the connction and restart the retrieval of the chunk
		self.tout = tout
		#Array that store the timouts for chunks
		self.tdef = {}
		
	def setup(self, alreadyRetrievedChunks):
		"""
			Using this function a download can be resumed, by passing the already downloaded chunks information
		"""
		for i in range(len(alreadyRetrievedChunks)):
			pass
		#Per far funzionare questo bisogna modificare la maniera con cui si seleziona il chunk da recuperare in schedule
		
	def schedule(self):
		"""
			Schedule the retrieval of various chunks at the same time
		"""
		if DEBUG: print "Retrieve: scheduling", self.retrieved, self.size, self.finished
		#Se ha completato(anche con errore eventualmente) l'intero file ritorna l'informazione al core
		if self.retrieved == self.size and not self.finished:
			if self.incomplete:
				if DEBUG: print "Retrieve: retrival complete but with something missing", self.key
				reactor.callFromThread(self.df.errback, self.key)
				self.cleanUp()
				self.finished = 1
			else:
				if DEBUG: print "Retrieve: retrival complete", self.key
				reactor.callFromThread(self.df.callback, self.key)
				self.cleanUp()
				self.finished = 1
			return
		#Se non deve schedulare niente di nuovo aspetta
		if self.retrieving >= self.par or self.retrieving+self.retrieved >= self.size or self.finished:
			if DEBUG: print "Retrieve: not possible to start a new retrieval:", self.key, self.retrieving,self.retrieved,self.size,self.finished
			return
		#Schedula le richieste in parallelo in base a quante ne sta gia' facendo
		if DEBUG: print "Retrieve: I have already in retrieve:", self.retrieving,"chunks for:",self.key
		for i in range(self.par - self.retrieving):
			chunk, node = self.nextCall()
			if not node:
				return
			self.retrievingChunks[chunk] = 1
			self.retrieving += 1
			self.tdef[chunk] = reactor.callLater(self.tout,self.timeout,chunk)
			if self.size == 1:
				df = self.httpClient.getContent(node[0], node[1], createKey(self.key,str(chunk)), size = self.updateSize, bitmap = self.updateBitmap)
			else:
				df = self.httpClient.getContent(node[0], node[1], createKey(self.key,str(chunk)), bitmap = self.updateBitmap)
			df.addCallback(self.retrieveCompleted)
			df.addErrback(self.errorWhileRetrieving)
			
	def passing(self):
		pass
		
	def updateBitmap(self,peer,bitmap):
		pass
		
	def startRetrieving(self):
		"""
			Start retrieving the content with the method selected
		"""
		if DEBUG: print "Retrieve: starting the retrieval"
		self.df = Deferred()
		reactor.callFromThread(self.schedule)
		return self.df
		
		
	def retrieveCompleted(self, (key, buf)):
		"""
			The retrieval of the chunk has been completed
		"""
		#Una volta che ho recuperato l'intero chunk l'ho inserisco nel mio buffer
		#Se ho recuperato tutti i chunk ho recuperato l'intero file
		if DEBUG: print "Retrieve: retrieved a chunk for key:", key
		tk, tc = divKey(key)
		if tk != self.key:
			if DEBUG: print 'Retrieve: Something strange is appening'
		if self.retrievedChunks[tc]:
			if DEBUG: print 'Retrieve: Already retrieved from someone else'
			self.retrieving -= 1
			reactor.callFromThread(self.schedule)
			return
		self.retrieved += 1
		self.retrieving -= 1
		self.retrievedChunks[tc] = 1
		if self.tdef.has_key(tc):
			try:
				self.tdef[tc].cancel()
			except Exception:
				pass
			del self.tdef[tc]
		#The buffer is passed to the lower level
		reactor.callFromThread(self.partCallback, self.key, tc, buf)
		reactor.callFromThread(self.schedule)
		
	def errorWhileRetrieving(self, failure):
		"""
			An error has occurred while retrieving the information
		"""
		#Se c'e' un errore nel recuperare un chunk devo provare a recuperarlo da un altro nodo
		#Se ho finito i nodi, devo ricominciare... o passo errore indietro
		#Si potrebbe aggiungere un'opzione per cui se lo stato ritornato e' file non esistente allora lo si toglie dalla mappa del nodo
		try:
			key,status = failure.value
		except Exception:
			return
		if DEBUG: print "Retrieve ERROR: retrieving key",key,"Reason:",status
		if status == '404' or status == 404:
			self.logger.logFailChunkCDN404(time.time())
		else:
			self.logger.logFailChunkCEPTO(time.time())
		#self.errorCalling()
		tk,tc = divKey(key)
		self.retrievingChunks[tc] = 0
		self.retrievedChunks[tc] = 0
		if self.tdef.has_key(tc):
			try:
				self.tdef[tc].cancel()
			except Exception:
				pass
			del self.tdef[tc]
		chunk, node = self.nextCall()
		self.retrievingChunks[chunk] = 1
		if not node:
			if DEBUG: print "Retrieve ERROR: strange behaviour after error"
			self.retrieving -= 1
			reactor.callFromThread(self.schedule)
			return
		df = self.httpClient.getContent(node[0], node[1], createKey(tk,str(chunk)), bitmap = self.updateBitmap)
		df.addCallback(self.retrieveCompleted)
		df.addErrback(self.errorWhileRetrieving)
		
	def nextCall(self):
		"""
			Determine which chunk to download and from wich node
		"""
		
	def cleanUp(self):
		"""
			Clean up everything that must be deleted
		"""
		
class RetrieveHTTP(RetrieveContent):
	"""
		Class that implements the retrieval using a list of cdn nodes
	"""
	def __init__(self, httpClient, key, partCallback, partErrback, sizeCallback, server, logger, size = 1, tout = 60):
		"""
			* httpClient: client utilized to download via http-download
			* key: key that represent the content to retrieve
			* partCallback: function to call as soon as a chunk is retrieved
			* partErrback: function to call as soon as a chunk has not been retrieved and it will not be retrieved
			* par: number of parallel requestes permitted during he retrieval
			* size: number of chunks that form the content
			* server: list of cdn nodes
			* sizeCallback: function to call after retrieving the first chunk to update the number of chunks that forms a content
		"""
		RetrieveContent.__init__(self,httpClient, key, partCallback, partErrback, logger, par = 1, size = size, tout = 60)
		self.server = server
		self.sizeCallback = sizeCallback
		#self.attempts = 0
		if DEBUG: print "Retrieve: Ready for retrieving via HTTP"
		
	def nextCall(self):
		"""
			Get next chunk to retrieve from the dht
		"""
		if DEBUG: print "RetrieveHTTP: selecting next chunk to retrieve from the cdn"
		rnd = random.randint(0,len(self.server)-1)
		return self.retrieved,self.server[rnd][0]
	
	#DEPRECATED
	#def errorCalling(self):
	#	self.attempts = (self.attempts+1)%len(self.server)
		
	def updateSize(self,size):
		self.size = size
		for i in range(1,size):
			self.retrievingChunks.append(0)
			self.retrievedChunks.append(0)
		self.sizeCallback(self.key, size)
	
	def timeout(self,chunk):
		if DEBUG: print "RetrieveHTTP ERROR: timeout while retrieving chunk:",chunk
		if self.retrievedChunks[chunk]:
			return
		#self.httpClient
		self.retrievingChunks[chunk] = 0
		if self.tdef.has_key(chunk):
			del self.tdef[chunk]
		self.retrieving -= 1
		#self.errorCalling()
		reactor.callFromThread(self.schedule)
		
class RetrieveP2P(RetrieveContent):
	"""
		Class that implements the retrieval of a content using a dummy p2p protocol
	"""
	def __init__(self, httpClient, key, partCallback, partErrback, peers, logger, par = 1, size = 1, REFT = 60, tout = 60):
		"""
			* httpClient: client utilized to download via http-download
			* key: key that represent the content to retrieve
			* partCallback: function to call as soon as a chunk is retrieved
			* partErrback: function to call as soon as a chunk has not been retrieved and it will not be retrieved
			* par: number of parallel requestes permitted during he retrieval
			* size: number of chunks that form the content
			* peers: list of peers to contact in the p2p
			* REFT: timeout for refreshing timeouts
		"""
		RetrieveContent.__init__(self,httpClient, key, partCallback, partErrback, logger, par = par, size = size, tout = 60)
		self.peers = peers
		self.first = 0
		self.bRef = None
		for p in self.peers:
			p['inuse'] = -1
		if DEBUG: print "RetrieveP2P: Ready for retrieving via P2P"
		self.REFT = REFT
		
	def startRetrieving(self):
		"""
			Start retrieving the content with the method selected
			In P2P activate updates for bitmaps
		"""
		if DEBUG: print "RetrieveP2P: starting the retrieval"
		#self.bRef = reactor.callLater(self.REFT, self.bitmapRefresh)
		return RetrieveContent.startRetrieving(self)
		
	def bitmapRefresh(self):
		"""
			For all the known nodes send a request for updating their bitmap
		"""
		for p in self.peers[:(len(self.peers)-2)]:
			self.httpClient.getHeader(p['address'][0], p['address'][1], self.key, bitmap = self.updateBitmap)
		self.bRef = reactor.callLater(self.REFT, self.bitmapRefresh)
		
	def nextCall(self):
		"""
			Select which chunk to retrieve and from which node
		"""
		if DEBUG: print "RetrieveP2P: selecting next chunk to retrieve from a node of the sworm starting from:", self.first,"for",self.key
		chunk = self.first - 1
		node = None
		free = 0
		for p in self.peers:
			if p['inuse'] == -1:
				free = 1
				break
		if not free:
			return (None,None)
		while not node:
			chunk += 1
			if chunk > self.size - 1:
				return (None,None)
			if not self.retrievingChunks[chunk] and not self.retrievedChunks[chunk]:
				node = self.whichNode(chunk)
		if DEBUG: print "RetrieveP2P: retrieve chunk",chunk,"from this node:",node,"for",self.key
		return (chunk,node)
		
	def whichNode(self, chunk):
		"""
			Return the closest available node that has the requested chunk
		"""
		for p in self.peers:
			if p['flag'].isOne(chunk) and p['inuse'] == -1:
				p['inuse'] = chunk
				return p['address']
		return None
		
	def retrieveCompleted(self, (key, buf)):
		"""
			The retrieval of the chunk has been completed
		"""
		#Una volta che ho recuperato l'intero chunk l'ho inserisco nel mio buffer
		#Se ho recuperato tutti i chunk ho recuperato l'intero file
		if DEBUG: print "RetrieveP2P: retrieved a chunk for key:", key
		tk, tc = divKey(key)
		if tk != self.key:
			if DEBUG: print 'RetrieveP2P ERROR: Something strange is appening'
		if self.retrievedChunks[tc]:
			if DEBUG: print 'RetrieveP2P: Already retrieved from someone else'
			self.stopUsing(tc)
			self.retrieving -= 1
			reactor.callFromThread(self.schedule)
			return
		self.retrieved += 1
		self.retrieving -= 1
		self.retrievedChunks[tc] = 1
		if self.tdef.has_key(tc):
			try:
				self.tdef[tc].cancel()
			except Exception:
				pass
			del self.tdef[tc]
		self.stopUsing(tc)
		if tc < self.first:
			if DEBUG: print 'RetrieveP2P: ERROR e sucesso qualcosa di strano'
		else:
			self.updateFirst()
		#The buffer is passed to the lower level
		reactor.callFromThread(self.partCallback, self.key, tc, buf)
		reactor.callFromThread(self.schedule)
		
	def errorWhileRetrieving(self, failure):
		"""
			An error has occurred while retrieving the information
		"""
		#Se c'e' un errore nel recuperare un chunk devo provare a recuperarlo da un altro nodo
		#Se ho finito i nodi, devo ricominciare... o passo errore indietro
		#Si potrebbe aggiungere un'opzione per cui se lo stato ritornato e' file non esistente allora lo si toglie dalla mappa del nodo
		try:
			key,status = failure.value
		except Exception:
			return
		if DEBUG: print "Retrieve ERROR: retrieving key",key,"Reason:",status
		tk,tc = divKey(key)
		if self.retrievedChunks[tc]:
			return
		self.retrievingChunks[tc] = 0
		self.retrievedChunks[tc] = 0
		node = self.stopUsing(tc)
		if status == '404' or status == 404:
			if node == self.peers[len(self.peers)-1]:
				self.logger.logFailChunkCDN404(time.time())
			else:
				self.logger.logFailChunkCEP404(time.time())
		else:
			if node == self.peers[len(self.peers)-1]:
				self.logger.logFailChunkCDNTO(time.time())
			else:
				self.logger.logFailChunkCEPTO(time.time())
		chunk, node = self.nextCall()
		self.retrievingChunks[chunk] = 1
		if self.tdef.has_key(tc):
			try:
				self.tdef[tc].cancel()
			except Exception:
				pass
			del self.tdef[tc]
		if not node:
			print "Retrieve ERROR: strange behaviour after error"
			self.retrieving -= 1
			reactor.callFromThread(self.schedule)
			return
		df = self.httpClient.getContent(node[0], node[1], createKey(tk,str(chunk)), bitmap = self.updateBitmap)
		df.addCallback(self.retrieveCompleted)
		df.addErrback(self.errorWhileRetrieving)
		
	def stopUsing(self, chunk):
		for p in self.peers:
			if p['inuse'] == chunk:
				p['inuse'] = -1
				#Ho tolto il return perche' si sono presentati casi di duplicazione... non dovrebbe accadere
				#Provare a toglierlo nel caso ci siano ancora errori
				return p['address']
		
	def updateFirst(self):
		if not self.retrievedChunks[self.first]:
			return
		i = self.first+1
		if i > self.size-1:
			return
		while self.retrievedChunks[i]:
			i += 1
			if i > self.size-1:
				return
		self.first = i
		
	def timeout(self,chunk):
		if DEBUG: print "RetrieveP2P ERROR: timeout while retrieving chunk:",chunk,"for",self.key
		if self.retrievedChunks[chunk]:
			return
		self.retrievingChunks[chunk] = 0
		self.stopUsing(chunk)
		self.retrieving -= 1
		reactor.callFromThread(self.schedule)
		if self.tdef.has_key(chunk):
			del self.tdef[chunk]
		
	def updateBitmap(self,peer,bitmap):
		for peer in self.peers:
			if peer['address'][0] == peer:
				peer['flag'].setMap(bitmap)
				return
				
	def cleanUp(self):
		"""
			Clean up everything that must be deleted
			Cancel the refresh schedule
		"""
		try:
			self.bRef.cancel()
		except Exception:
			if DEBUG: print "RetrieveP2P: ERROR cleaning up", `sys.exc_info()`
		
#Decidere se serve una classe a parte per la cdn o meno
		

class test:
	def __init__(self):
		from cep.statsLogger import StatsLogger
		import sys
		s,init,tot = divFlag(2622721)
		m1 = Bitmap()
		m1.editFromPerc(init,tot,640,2)
		m2 = Bitmap()
		m2.editFromPerc(init,tot,640,2)
		m3 = Bitmap()
		m3.editFromPerc(init,tot,640,2)
		m4 = Bitmap()
		m4.editFromPerc(init,tot,640,2)
		self.c = CepWebClient('/home/wontoniii/coast/tmp/cepcache/',reactor)
		peers = [{'flag': m1, 'address': ['localhost', 10111]}, {'flag': m2, 'address': ['localhost', 10112]}, {'flag': m3, 'address': ['localhost', 10113]}, {'flag': m4, 'address': ['localhost', 10114]}]
		#peers = [[['localhost', 10111],1], [['localhost', 10112],1]]
		self.logger = StatsLogger("prova.txt")
		if sys.argv[1] == '1':
			self.retrieve = RetrieveP2P(self.c, str(101262), self.chunkRec, self.chunkErr, peers, self.logger, size = 640, par = 4)
		else:
			self.retrieve = RetrieveHTTP(self.c, str(101262), self.chunkRec, self.chunkErr, None, peers, self.logger, size = 640)
		
	def start(self):
		self.logger.startLogging(0)
		df = self.retrieve.startRetrieving()
		df.addCallback(self.finished) 
		
	def chunkRec(self, key, chunk, buf):
		print 'Finished to retrieve content:', key, 'chunk:',chunk
		#print buf.getData()
		
	def chunkErr(self, key, chunk):
		print 'Impossible to retrieve content:', key, 'chunk:',chunk
		
	def finished(self, key):
		print 'Finished to retrieve key:', key
		reactor.stop()

if __name__=="__main__":
	t = test()
	t.start()
	reactor.run()
