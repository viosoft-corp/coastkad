"""
	Module where are defined the modules used to generate the web client used by a cep node
"""

from twisted.internet.defer import Deferred
from twisted.internet.protocol import Protocol
from twisted.web.client import Agent
from twisted.web.http_headers import Headers

import sys,os

from cep.buffer import Buffer

DEBUG = True
HTTP_ERRORS = [204,400,401,402,403,404,500,501,502,503]

class CepWebClientProtocol(Protocol):
	"""
		Class that extend Protocol and add the functionalities required by a cep node
	"""
	def __init__(self, key, callback, finished, reac):
		"""
			* key: COI+n. chunk
			* callback: function to call once retrieved a packet
			* finished: function to call once the retrieval of a content
			* reac: reactor to be used
		"""
		self.key = key
		self.callback = callback
		self.finished = finished
		self.reactor = reac
	
	def dataReceived(self, bytes):
		self.reactor.callFromThread(self.callback, self.key, bytes)
		
	def connectionLost(self, reason):
		Protocol.connectionLost(self,reason)
		self.reactor.callFromThread(self.finished, self.key, reason)
		try:
			self.transport.stopProducing()
		except Exception:
			if DEBUG: print "CepWebClientProtocol ERROR: ", `sys.exc_info()`


class CepWebClient:
	"""
		Class that implements the web client used by a cep node
	"""
	def __init__(self, bufferPath, react = None):
		"""
			* bufferPath: buffer used to store the contents while retrieving
			* react: reactor to be used
		"""
		if not react:
			from twisted.internet import reactor
			self.reactor = reactor
		else:
			self .reactor = react
		self.agent = Agent(self.reactor)
		self.bufferPath = bufferPath
		try:
			os.stat(self.bufferPath)
		except Exception:
			if DEBUG: print 'CepWebClient ERRORE: Errore inizializzazione'
			os.makedirs(self.bufferPath)
		self.retrieving = {}
		self.buffer = {}
		self.size = {}
		self.bitmap = {}
		self.nodes = {}
		self.Headers = Headers({'User-Agent': ['CEP Web Client']})
		
	def getContent(self, node, port, key, size = None, bitmap = None):
		try:
			if DEBUG: print 'WebClient: get content:',key,'From:',node,port
			URI = 'http://'+node+':'+str(port)+'/'+key
			self.nodes[key] = node
		except Exception:
			URI = 'http://'+node[0]+':'+str(port)+'/'+key
			self.nodes[key] = node[0]
		self.retrieving[key] = Deferred()
		self.size[key] = size
		self.bitmap[key] = bitmap
		try:
			df = self.agent.request('GET',URI,self.Headers,None)
		except Exception:
			df = self.retrieving[key]
			del self.retrieving[key]
			self.reactor.callFromThread(df.errback, (key, 0))
		df.addCallback(self.gotResponse,key)
		df.addErrback(self.gotError,key)
		return self.retrieving[key]
		
	def getHeader(self, node, port, key, bitmap = None):
		try:
			if DEBUG: print 'WebClient: get header:',key,'From:',node,port
			URI = 'http://'+node+':'+str(port)+'/'+key
			self.nodes[key] = node
		except Exception:
			URI = 'http://'+node[0]+':'+str(port)+'/'+key
			self.nodes[key] = node[0]
		self.bitmap[key] = bitmap
		try:
			df = self.agent.request('HEAD',URI,self.Headers,None)
		except Exception:
			return -1
		df.addCallback(self.gotHeader,key)
		df.addErrback(self.gotErrorHeader,key)
		return 1
		
	def gotResponse(self, response, key):
		if not self.retrieving.has_key(key):
			return
		if response.code in HTTP_ERRORS:
			if DEBUG: print 'WebClient ERROR: error code from the server:',response.code
			df = self.retrieving[key]
			del self.retrieving[key]
			self.reactor.callFromThread(df.errback, (key, response.code))
			return
		if response.headers.hasHeader("Content-Size"):
			try:
				size = int(response.headers.getRawHeaders("Content-Size")[0])
				if self.size.has_key(key):
					if self.size[key]:
						self.size[key](size)
					del self.size[key]
			except Exception:
				if DEBUG: print 'WebClient ERROR: Content-Size', `sys.exc_info()`
				raise Exception
		if response.headers.hasHeader("Content-bitmap"):
			try:
				bitmap = int(response.headers.getRawHeaders("Content-bitmap")[0])
				if self.bitmap.has_key(key):
					if self.bitmap[key]:
						self.bitmap[key](self.nodes[key], bitmap)
					del self.bitmap[key]
			except Exception:
				if DEBUG: print 'WebClient ERROR: Content-bitmap', `sys.exc_info()`
				raise Exception
		self.buffer[key] = Buffer(self.bufferPath+key)
		response.deliverBody(CepWebClientProtocol(key, self.gotData, self.finished, self.reactor))
	
	def gotHeader(self, response, key):
		if response.code in HTTP_ERRORS:
			if DEBUG: print 'WebClient ERROR: error code from the server:',response.code
			self.bitmap[key](self.nodes[key], 0, error = response.code)
		elif response.headers.hasHeader("Content-bitmap"):
			if DEBUG: print 'WebClient: received header for:',key
			try:
				bitmap = int(response.headers.getRawHeaders("Content-bitmap")[0])
				if self.bitmap.has_key(key):
					if self.bitmap[key]:
						self.bitmap[key](self.nodes[key], bitmap)
					del self.bitmap[key]
			except Exception:
				if DEBUG: print 'WebClient ERROR: Content-bitmap response', `sys.exc_info()`
		#response.deliverBody(CepWebClientProtocol(key, self.gotDataHeader, self.finishedHeader, self.reactor))
		if self.nodes.has_key(key):
			del self.nodes[key]
		if self.bitmap.has_key(key):
			del self.bitmap[key]
	
	def gotError(self, fail, key):
		if self.retrieving.has_key(key):
			df = self.retrieving[key]
			del self.retrieving[key]
			self.reactor.callFromThread(df.errback, (key, fail))
			
	def gotErrorHeader(self, fail, key):
		if DEBUG: print 'WebClient ERROR: Header-bitmap error in transmission', fail
		if self.nodes.has_key(key):
			del self.nodes[key]
		if self.bitmap.has_key(key):
			del self.bitmap[key]
		
	def gotData(self,key,data):
		#I dati sono aggiunti al buffer, e rimandati indietro nel momento in cui il chunk e' stato recuperato
		if data and self.buffer.has_key(key):
			self.buffer[key].append(data)
		
	def finished(self, key, status):
		#Fare l'analisi dello stato di uscita e verificare che sia andato tutto correttamente
		if DEBUG: print "WebClient: finished to retrieve:",key,status.value
		df = None
		buf = None
		if self.nodes.has_key(key):
			del self.nodes[key]
		if self.size.has_key(key):
			del self.size[key]
		if self.bitmap.has_key(key):
			del self.bitmap[key]
		if self.retrieving.has_key(key):
			df = self.retrieving[key]
			del self.retrieving[key]
		if self.buffer.has_key(key):
			buf = self.buffer[key]
			del self.buffer[key]
		if df and buf:
			self.reactor.callFromThread(df.callback, (key, buf))
			
	def gotDataHeader(self, key, data):
		pass
		
	def finishedHeader(self, key, status):
		pass


class testClient:
	from twisted.internet import reactor
	
	def __init__(self, filename, ref):
		self.c = CepWebClient('/tmp/')
		self.filename = filename
		self.ref = ref
		self.n = 0
		from cep.utils import createKey
		self.createKey = createKey
		
	def runTest(self):
		reactor.callLater(0,self.loop)
		
	def loop(self):
		df = None
		if sys.argv[3] == '1':
			df = self.c.getContent('localhost',10111, self.createKey(self.filename,self.n))
			df.addCallback(self.retrieved)
			df.addErrback(self.error)
			self.n += 1	
			reactor.callLater(self.ref,self.loop)
		else:
			df = self.c.getHeader('localhost',10111, self.filename, bitmap = self.bitmap)
			reactor.callLater(self.ref,self.loop)	
		
		
	def retrieved(self, (key, buf)):
		print 'Finished to retrieve content:', key
		
	def error(self, failure):
		print "ERROR ",failure
		
	def bitmap(self, key, buf):
		print 'Finished to retrieve content:', key,buf
		

if __name__ == "__main__":
	from twisted.internet import reactor
	t = testClient(sys.argv[1], int(sys.argv[2]))
	t.runTest()
	reactor.run()
