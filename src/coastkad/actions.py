
"""Details of how to perform actions on remote peers."""

from datetime import datetime

from twisted.internet import reactor, defer
from twisted.python import log

from khash import intify, newTID
from ktable import K
from util import uncompact
from coastkad.kconf import config

TRACK = config.getboolean('KDHT','TRACK')
DEBUG = config.getboolean('DEFAULT','DEBUG')

class ActionBase:
	"""Base class for some long running asynchronous proccesses like finding nodes or values.
	
	@type caller: L{khashmir.Khashmir}
	@ivar caller: the DHT instance that is performing the action
	@type target: C{string}
	@ivar target: the target of the action, usually a DHT key
	@type config: C{dictionary}
	@ivar config: the configuration variables for the DHT
	@type action: C{string}
	@ivar action: the name of the action to call on remote nodes
	@type stats: L{stats.StatsLogger}
	@ivar stats: the statistics modules to report to
	@type num: C{long}
	@ivar num: the target key in integer form
	@type queried: C{dictionary}
	@ivar queried: the nodes that have been queried for this action,
		keys are node IDs, values are the node itself
	@type answered: C{dictionary}
	@ivar answered: the nodes that have answered the queries
	@type failed: C{dictionary}
	@ivar failed: the nodes that have failed to answer the queries
	@type found: C{dictionary}
	@ivar found: nodes that have been found so far by the action
	@type sorted_nodes: C{list} of L{node.Node}
	@ivar sorted_nodes: a sorted list of nodes by there proximity to the key
	@type results: C{dictionary}
	@ivar results: keys are the results found so far by the action
	@type desired_results: C{int}
	@ivar desired_results: the minimum number of results that are needed
		before the action should stop
	@type callback: C{method}
	@ivar callback: the method to call with the results
	@type outstanding: C{dictionary}
	@ivar outstanding: the nodes that have outstanding requests for this action,
		keys are node IDs, values are the number of outstanding results from the node
	@type outstanding_results: C{int}
	@ivar outstanding_results: the total number of results that are expected from
		the requests that are currently outstanding
	@type finished: C{boolean}
	@ivar finished: whether the action is done
	@type started: C{datetime.datetime}
	@ivar started: the time the action was started at
	@type sort: C{method}
	@ivar sort: used to sort nodes by their proximity to the target
	"""
	
	def __init__(self, caller, target, callback, config, stats, action, num_results = None):
		"""Initialize the action.
		
		@type caller: L{khashmir.Khashmir}
		@param caller: the DHT instance that is performing the action
		@type target: C{string}
		@param target: the target of the action, usually a DHT key
		@type callback: C{method}
		@param callback: the method to call with the results
		@type config: C{dictionary}
		@param config: the configuration variables for the DHT
		@type stats: L{stats.StatsLogger}
		@param stats: the statistics gatherer
		@type action: C{string}
		@param action: the name of the action to call on remote nodes
		@type num_results: C{int}
		@param num_results: the minimum number of results that are needed before
			the action should stop (optional, defaults to getting all the results)
		
		"""
		
		self.caller = caller
		self.target = target
		self.config = config
		self.action = action
		self.stats = stats
		self.stats.startedAction(action)
		self.num = intify(target)
		self.queried = {}
		self.answered = {}
		self.failed = {}
		self.found = {}
		self.sorted_nodes = []
		self.results = {}
		self.desired_results = num_results
		self.callback = callback
		self.outstanding = {}
		self.outstanding_results = 0
		self.finished = False
		self.started = datetime.now()
		#ID for storing queries informations
		self.TID = newTID()
		#Dictionary containing the number of hops that have been made to reach a node(node.id are the key)
		self.hops = {}
		#Dictionary containing which node gave us the information about a node(node.id are the key)
		self.fathers = {}
		self.hops[caller.node.id] = 0
		self.fathers[caller.node.id] = None
	
		def sort(a, b, num=self.num):
			"""Sort nodes relative to the ID we are looking for."""
			x, y = num ^ a.num, num ^ b.num
			if x > y:
				return 1
			elif x < y:
				return -1
			return 0
		self.sort = sort

	#{ Main operation
	def goWithNodes(self, nodes):
		"""Start the action's process with a list of nodes to contact."""
		if DEBUG: print 'ActionBase goWithNodes'
		#This timestamp is usefull for performance analysis
		self.started = datetime.now()
		for node in nodes:
			if DEBUG: 'Starting node: '+`node`
			self.found[node.id] = node
			if TRACK:
				self.hops[node.id] = 1
				self.fathers[node.id] = self.caller.node.id
		self.sortNodes()
		self.schedule()
	
	def schedule(self):
		"""Schedule requests to be sent to remote nodes."""
		if DEBUG: print 'ActionBase schedule'
		if self.finished:
			return
		
		# Get the nodes to be processed
		nodes = self.getNodesToProcess()
		
		# Check if we are already done
		if nodes is None or (self.desired_results and
							 ((len(self.results) >= abs(self.desired_results)) or
							  (self.desired_results < 0 and
							   len(self.answered) >= self.config['STORE_REDUNDANCY']))):
			self.finished = True
			result = self.generateResult()
			reactor.callLater(0, self.callback, *result)
			return

		# Check if we have enough outstanding results coming
		if (self.desired_results and 
			len(self.results) + self.outstanding_results >= abs(self.desired_results)):
			return
		
		# Loop for each node that should be processed
		for node in nodes:
			# Don't send requests twice or to ourself
			if node.id not in self.queried:
				self.queried[node.id] = 1
				
				# Get the action to call on the node
				if node.id == self.caller.node.id:
					try:
						f = getattr(self.caller, 'krpc_' + self.action)
					except AttributeError:
						log.msg("%s doesn't have a %s method!" % (node, 'krpc_' + self.action))
						continue
				else:
					try:
						f = getattr(node, self.action)
					except AttributeError:
						log.msg("%s doesn't have a %s method!" % (node, self.action))
						continue

				# Get the arguments to the action's method
				try:
					args, expected_results = self.generateArgs(node)
				except ValueError:
					continue

				# Call the action on the remote node
				self.outstanding[node.id] = expected_results
				self.outstanding_results += expected_results
				df = defer.maybeDeferred(f, *args)
				reactor.callLater(0, df.addCallbacks,
								  *(self.gotResponse, self.actionFailed),
								  **{'callbackArgs': (node, ),
									 'errbackArgs': (node, )})
						
			# We might have to stop for now
			if (len(self.outstanding) >= self.config['CONCURRENT_REQS'] or
				(self.desired_results and
				 len(self.results) + self.outstanding_results >= abs(self.desired_results))):
				break
			
		assert self.outstanding_results >= 0

		# If no requests are outstanding, then we are done
		if len(self.outstanding) == 0:
			self.finished = True
			result = self.generateResult()
			reactor.callLater(0, self.callback, *result)

	def gotResponse(self, dict, node):
		"""Receive a response from a remote node."""
		if DEBUG: print 'ActionBase gotResponse'
		if self.finished or self.answered.has_key(node.id) or self.failed.has_key(node.id):
			# a day late and a dollar short
			return
		try:
			# Process the response
			self.processResponse(dict)
		except Exception, e:
			# Unexpected error with the response
			log.msg("action %s failed on %s/%s: %r" % (self.action, node.host, node.port, e))
			if node.id != self.caller.node.id:
				self.caller.nodeFailed(node)
			self.failed[node.id] = 1
		else:
			self.answered[node.id] = 1
			if node.id != self.caller.node.id:
				reactor.callLater(0, self.caller.insertNode, node)
		if self.outstanding.has_key(node.id):
			self.outstanding_results -= self.outstanding[node.id]
			del self.outstanding[node.id]
		self.schedule()

	def actionFailed(self, err, node):
		"""Receive an error from a remote node."""
		if DEBUG: print 'ActionBase actionFailed'
		log.msg("action %s failed on %s/%s: %s" % (self.action, node.host, node.port, err.getErrorMessage()))
		if node.id != self.caller.node.id:
			self.caller.nodeFailed(node)
		self.failed[node.id] = 1
		if self.outstanding.has_key(node.id):
			self.outstanding_results -= self.outstanding[node.id]
			del self.outstanding[node.id]
		self.schedule()
	
	def handleGotNodes(self, nodes, father = 0):
		"""Process any received node contact info in the response.
		
		Not called by default, but suitable for being called by
		L{processResponse} in a recursive node search.
		"""
		if DEBUG: print 'ActionBase handleGotNodes'
		if nodes and type(nodes) != list:
			raise ValueError, "got a malformed response, from bittorrent perhaps"
		for compact_node in nodes:
			node_contact = uncompact(compact_node)
			node = self.caller.Node(node_contact)
			if not self.found.has_key(node.id):
				if TRACK:
					self.hops[node.id] = self.hops[father] + 1
					self.fathers[node.id] = father
				self.found[node.id] = node

	def sortNodes(self):
		"""Sort the nodes, if necessary.
		
		Assumes nodes are never removed from the L{found} dictionary.
		"""
		if DEBUG: print 'ActionBase sortNodes'
		if len(self.sorted_nodes) != len(self.found):
			self.sorted_nodes = self.found.values()
			self.sorted_nodes.sort(self.sort)
				
	#{ Subclass for specific actions
	def getNodesToProcess(self):
		"""Generate a list of nodes to process next.
		
		This implementation is suitable for a recurring search over all nodes.
		It will stop the search when the closest K nodes have been queried.
		It also prematurely drops requests to nodes that have fallen way behind.
		
		@return: sorted list of nodes to query, or None if we are done
		"""
		if DEBUG: print 'ActionBase getNodesToProcess'
		# Find the K closest nodes that haven't failed, count how many answered
		self.sortNodes()
		closest_K = []
		ans = 0
		for node in self.sorted_nodes:
			if node.id not in self.failed:
				closest_K.append(node)
				if node.id in self.answered:
					ans += 1
				if len(closest_K) >= K:
					break
		
		# If we have responses from the K closest nodes, then we are done
		if ans >= K:
			log.msg('Got the answers we need, aborting search')
			return None
		
		# Check the oustanding requests to see if they are still closest
		for id in self.outstanding.keys():
			if self.found[id] not in closest_K:
				# Request is not important, allow another to go
				log.msg("Request to %s/%s is taking too long, moving on" %
						(self.found[id].host, self.found[id].port))
				self.outstanding_results -= self.outstanding[id]
				del self.outstanding[id]
		
		if DEBUG: print 'Closest_k: '+`closest_K`
		
		return closest_K
	
	def generateArgs(self, node):
		"""Generate the arguments to the node's action.
		
		Also return the number of results expected from this request.
		
		@raise ValueError: if the node should not be queried
		"""
		if DEBUG: print 'ActionBase generateArgs'
		return (self.caller.node.id, self.target), 0
	
	def processResponse(self, dict):
		"""Process the response dictionary received from the remote node."""
		if DEBUG: print 'ActionBase processResponse'
		self.handleGotNodes(dict['nodes'], dict['id'])

	def generateResult(self, nodes):
		"""Create the final result to return to the L{callback} function."""
		if DEBUG: print 'ActionBase generateResult'
		self.stats.completedAction(self.action, self.started)
		return []
		

class FindNode(ActionBase):
	"""Find the closest nodes to the key."""

	def __init__(self, caller, target, callback, config, stats, action="find_node"):
		ActionBase.__init__(self, caller, target, callback, config, stats, action)

	def processResponse(self, dict):
		"""Save the token received from each node."""
		if DEBUG: print 'FindNode processResponse'
		if dict["id"] in self.found:
			self.found[dict["id"]].updateToken(dict.get('token', ''))
		self.handleGotNodes(dict['nodes'], dict['id'])

	def generateResult(self):
		"""Result is the K closest nodes to the target."""
		if DEBUG: print 'FindNode generateResult'
		self.sortNodes()
		self.stats.completedAction(self.action, self.started)
		closest_K = []
		for node in self.sorted_nodes:
			if node.id not in self.failed:
				closest_K.append(node)
				if len(closest_K) >= K:
					break
		if DEBUG:
			for node in closest_K:
				print `node`
		return (closest_K, )
	
	
class FindValue(ActionBase):
	"""Retrieve values from a list of nodes."""
	
	def __init__(self, caller, target, num_results, callback, config, stats, action="find_value"):
		ActionBase.__init__(self, caller, target, callback, config, stats, action)
		
	def goWithNodes(self, nodes, values):
		"""Start the action's process with a list of nodes to contact."""
		if DEBUG: print 'FindValue goWithNodes'
		#This timestamp is usefull for performance analysis
		self.started = datetime.now()
		for node in nodes:
			self.found[node.id] = node
			if TRACK:
				self.hops[node.id] = 1
				self.fathers[node.id] = self.caller.node.id
		
		for value in values:
			self.results[value] = 1
			if TRACK:
				#print 'Lo stampo da FIND VALUE: ',value
				self.fathers[value] = self.caller.node.id
				self.hops[value] = 0
		self.sortNodes()
		self.schedule()

	def processResponse(self, dict):
		"""Save the returned values, calling the L{callback} each time there are new ones."""
		if DEBUG: print 'FindValue processResponse: '+`dict`
		if dict.has_key('nodes'):
			#if DEBUG: print 'Got this nodes '+`dict['nodes']`+' from '+`dict['_krpc_sender']`
			self.handleGotNodes(dict['nodes'], dict['id'])
		elif dict.has_key('values'):
			#if DEBUG: print 'Got this values '+`dict['values']`+' from '+`dict['_krpc_sender']`
			def x(y, z=self.results):
				if not z.has_key(y):
					z[y] = 1
					if TRACK:
						self.hops[y] = self.hops[dict['id']]
						self.fathers[y] = dict['id']
					return y
				else:
					return None
			z = len(dict['values'])###????A che serve????
			v = filter(None, map(x, dict['values']))
			if len(v):
				reactor.callLater(0, self.callback, self.target, v)
		else:
			if DEBUG: print 'Error in msg receiced, no nodes nore values'

	def generateResult(self):
		"""Results have all been returned, now send the empty list to end the action."""
		if DEBUG: print 'FindValue generateResults'
		self.stats.completedAction(self.action, self.started)
		return (self.target, [], self.TID, self.hops, self.fathers)
		

class StoreValue(ActionBase):
	"""Store a value in a list of nodes."""

	def __init__(self, caller, target, value, num_results, callback, config, stats, action="store_value"):
		"""Initialize the action with the value to store.
		
		@type value: C{string}
		@param value: the value to store in the nodes
		"""
		ActionBase.__init__(self, caller, target, callback, config, stats, action, num_results)
		self.value = value
		
	def getNodesToProcess(self):
		"""Nodes are never added, always return the same sorted list."""
		if DEBUG: print 'StoreValue getNodesToProcess'
		return self.sorted_nodes

	def generateArgs(self, node):
		"""Args include the value to store and the node's token."""
		if DEBUG: print 'StoreValue generateArgs'
		if node.token:
			return (self.caller.node.id, self.target, self.value, node.token), 1
		else:
			raise ValueError, "Don't store at this node since we don't know it's token"

	def processResponse(self, dict):
		"""Save the response, though it should be nothin but the ID."""
		if DEBUG: print 'StoreValue processResponse'
		self.results[dict["id"]] = dict
	
	def generateResult(self):
		"""Return all the response IDs received."""
		if DEBUG: print 'StoreValue generateResult'
		self.stats.completedAction(self.action, self.started)
		return (self.target, self.value, self.results.values())
