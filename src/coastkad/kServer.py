from interfaces import IKServerInterface
from kServerProtocol import KServerProtocolTCP, KSRPC
from datetime import datetime, timedelta
from random import randrange, shuffle
from sha import sha
from copy import copy
import os, re

from twisted.internet.defer import Deferred
from twisted.internet.base import DelayedCall
from twisted.internet import protocol, reactor
from twisted.python import log
from twisted.trial import unittest
from zope.interface import implements
from coastkad.kconf import config
DEBUG = config.getboolean('DEFAULT','DEBUG')

class KServer(protocol.Factory):
	
	implements(IKServerInterface)
	
	def __init__(self,core,dht,msgHandler,protocol):
		self.core = core
		self.msgHandler = msgHandler
		self.dht = dht
		self.protocol = protocol
		self.protocol.protocol = msgHandler
		self.protocol.server = self
	
	def turnOn(self):
		"""After this command the node start working(join the DHT and start receiving requests)"""
		df = Deferred()
		df= self.dht.join(df)
		return df
		
	def turnOff(self):
		"""Make the node leave the DHT"""
		try:
			self.dht.leave()
		except DHTError, error:
			return error
		else:
			df = Deferred()
			return 1
		#Capire bene cosa fa nel caso di errore
		
	def getValue(self, key):
		"""Get value from DHT by a given key"""
		return self.dht.getValue(key)
		
	def storeValue(self, key, value):
		"""Store the value in the DHT"""
		return self.dht.storeValue(key,value)
		
	def getStatus(self):
		return [self.dht.getJoined(),self.dht.getId(),self.dht.getAddrs()]
	
	def getStats(self):
		"""Future use"""
		return self.dht.getStats()
