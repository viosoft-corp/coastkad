from twisted.internet.defer import Deferred
from twisted.protocols import basic
from bencode import bencode, bdecode
from twisted.internet import protocol
from twisted.internet import reactor
from twisted.python import log
from coastkad.kconf import config

import time

import sys

from bencode import bencode, bdecode, BencodeError

DEBUG = config.getboolean('DEFAULT','DEBUG')

#Protocol commands
STORE = 's'
GET = 'g'
TON = 't'
TOFF = 'f'
OK = 'o'
NOK = 'n'
PING = 'p'
GS = 'm'
GSTS = 'a'


###########################################################
#####    NOT WORKING   ###################################
###########################################################
#UPD protocol need some modifies. As it is, it will not work
class KServerProtocolUDP(protocol.DatagramProtocol):
	def __init__(self, protocol, server):
		self.connections = {}
		self.protocol = protocol
		self.server = server
		
	def datagramReceived(self, datagram, addr):
		if DEBUG: print 'Datagram received: ', `addr`, `datagram`
		df = None
		msg = bdecode(datagram)
		c = self.connectionForAddr(addr)
		if msg['cmd'] == 's':
			df = c.storeValue()
		elif msg['cmd'] == 'g':
			df = c.getValue()
		elif msg['cmd'] == 't':
			df = c.turnOn()
		elif msg['cmd'] == 'f':
			df = c.turnOff()
		elif msg['cmd'] == 'p':
			df = c.ping()
		df.addCallback(self.sendReplay)
		df.addErrback(self.sendError)
	
	def connectionForAddr(self, addr):
		if not self.connections.has_key(addr):
			conn = self.protocol(self.server, addr)
			self.connections[addr] = conn
		else:
			conn = self.connections[addr]
		return conn
		
	def sendReplay(self, res):
		addr = res[0]
		msg = res[1]
		self.transport.write(msg, addr)
	
	def sendError(self, fail):
		pass
	
	#Crea la connessione con un host
	def makeConnection(self, transport):
		protocol.DatagramProtocol.makeConnection(self, transport)
		tup = transport.getHost()
		self.addr = (tup.host, tup.port)
		if DEBUG: print 'My address is: ', self.addr
###########################################################
#####    NOT WORKING   ###################################
###########################################################
		
class KServerProtocolTCP(protocol.Protocol):
	def __init__(self):
		self.msgs = {}
	
	def connectionMade(self):
		if DEBUG: print 'qualcosa bisognerebbe fare nel momento in cui si crea la connessione'
		
	def dataReceived(self, datagram):
		if DEBUG: print 'Packet received: ', `datagram`
		log.msg('Packet received on time: '+`time.time()`)
		df = None
		try:
			msg = bdecode(datagram)
		except BencodeError:
			if DEBUG: print 'Bad bencoded data'
			log.err('ERROR: Bad bencoded data')
		else:
			self.msgs[msg['mid']] = self.protocol(self.server,msg)
			if msg['cmd'] == STORE:
				df = self.msgs[msg['mid']].storeValue()
			elif msg['cmd'] == GET:
				df = self.msgs[msg['mid']].getValue()
			elif msg['cmd'] == TON:
				df = self.msgs[msg['mid']].turnOn()
			elif msg['cmd'] == TOFF:
				df = self.msgs[msg['mid']].turnOff()
			elif msg['cmd'] == PING:
				df = self.msgs[msg['mid']].ping()
			elif msg['cmd'] == GS:
				df = self.msgs[msg['mid']].getState()
			elif msg['cmd'] == GSTS:
				df = self.msgs[msg['mid']].getStats()
			df.addCallback(self.sendReplay)
	
	def connectionLost(self, reason):
		if DEBUG: print 'Connessione persa per questa ragione: ', reason
		
	def sendReplay(self, res):
		msg = res[1]
		self.msgs.pop(msg['mid'])
		if DEBUG: print 'Sending this replay: ', msg
		self.transport.write(bencode(msg))
		
	
		
class KSRPC:
	def __init__(self, server, msg, addr = None):
		self.server = server
		self.addr = addr
		self.msg = msg
	
	def storeValue(self):
		log.msg('STORE OPERATION STARTED ON KEY: '+`self.msg['value']['key']`)
		df = self.server.storeValue(self.msg['value']['key'],self.msg['value']['value'])
		df.addCallbacks(self._storeValue,self._storeValueError)
		return df
		
	def _storeValue(self,result):
		rep = {'mid':self.msg['mid'],'cmd':OK, 'value':result}
		log.msg('STORE OPERATION FOR KEY: '+`self.msg['value']['key']`+' SUCCEDED. REPLY: '+`rep['value']`)
		return [self.addr,rep]
	
	def _storeValueError(self,error):
		rep = {'mid':self.msg['mid'],'cmd':NOK, 'value':error.getErrorMessage()}
		log.msg('ERROR: STORE OPERATION FOR KEY: '+`self.msg['value']['key']`+' FAILED. REPLY: '+`rep['value']`)
		return [self.addr,rep]
	
	def getValue(self):
		log.msg('GET OPERATION STARTED ON KEY: '+`self.msg['value']`)
		#print self.msg['value']
		df = self.server.getValue(self.msg['value'])
		df.addCallbacks(self._getValue,self._getValueError)
		return df
		
	def _getValue(self,result):
		rep = {'mid':self.msg['mid'],'cmd':OK, 'value':result}
		log.msg('GET OPERATION FOR KEY: '+`self.msg['value']['key']`+' SUCCEDED. REPLY: '+`rep['value']`)
		return [self.addr,rep]
	
	def _getValueError(self,error):
		rep = {'mid':self.msg['mid'],'cmd':NOK, 'value':error.getErrorMessage()}
		log.msg('ERROR: GET OPERATION FOR KEY: '+`self.msg['value']['key']`+' FAILED. REPLY: '+`rep['value']`)
		return [self.addr,rep]
	
	def turnOn(self):
		df = self.server.turnOn()
		df.addCallbacks(self._turnOn,self._turnOnError)
		return df
		
	def _turnOn(self, result):
		"""Prepare the message for replay to the request"""
		rep = {'mid':self.msg['mid'],'cmd':OK, 'value':result}
		return [self.addr,rep]
	
	def _turnOnError(self, result):
		"""Prepare the message for replay to the request"""
		mid = result
		rep = {'mid':self.msg['mid'],'cmd':NOK, 'value':result}
		return [self.addr,rep]
	
	def turnOff(self):
		ret = self.server.turnOff()
		rep = {}
		if ret == 1:
			rep['value'] = 'Shutdown completed'
			rep['cmd'] = OK
		else:
			rep['value'] = ret
			rep['cmd'] = NOK
		rep['mid'] = self.msg['mid']
		df = Deferred()
		reactor.callLater(0,df.callback,[self.addr,rep])
		reactor.callLater(10,reactor.stop)
		return df
	
	def ping(self):
		df = Deferred()
		rep = self.msg
		reactor.callLater(0,df.callback,[self.addr,rep])
		return df
	
	def getState(self):
		status = self.server.getStatus()
		rep = {}
		rep['mid'] = self.msg['mid']
		rep['cmd'] = OK
		rep['value'] = {'joined':status[0],'addr':status[2],'id':status[1]}
		df = Deferred()
		reactor.callLater(0,df.callback,[self.addr,rep])
		return df
		
	def getStats(self):
		stats = self.server.getStats()
		rep = {}
		rep['mid'] = self.msg['mid']
		rep['cmd'] = OK
		rep['value'] = stats
		df = Deferred()
		reactor.callLater(0,df.callback,[self.addr,rep])
		return df

class PingRPC:
	def __init__(self, server, msg, addr = None):
		self.server = server
		self.addr = addr
		self.msg = msg
	
	def storeValue(self):
		df = Deferred()
		rep = bencode({'mid':self.msg['mid'],'cmd':OK, 'value':'ping store request'})
		reactor.callLater(2,df.callback,[self.addr,rep])
		return df
	
	def getValue(self):
		df = Deferred()
		rep = bencode({'mid':self.msg['mid'],'cmd':OK, 'value':'ping get request'})
		reactor.callLater(2,df.callback,[self.addr,rep])
		return df
	
	def turnOn(self):
		df = Deferred()
		rep = bencode({'mid':self.msg['mid'],'cmd':OK, 'value':'ping turnon request'})
		reactor.callLater(2,df.callback,[self.addr,rep])
		return df
	
	def turnOff(self):
		df = Deferred()
		rep = bencode({'mid':self.msg['mid'],'cmd':OK, 'value':'ping turnoff request'})
		reactor.callLater(2,df.callback,[self.addr,rep])
		return df
	
	def ping(self):
		df = Deferred()
		rep = bencode({'mid':self.msg['mid'],'cmd':OK, 'value':self.msg['value']})
		reactor.callLater(2,df.callback,[self.addr,rep])
		return df
	
