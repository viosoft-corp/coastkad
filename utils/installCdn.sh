if ! mkdir ~/coast/tmp
then
	echo "Dir already existed";
fi
if ! mkdir ~/coast/tmp/cdncache
then
	echo "Dir already existed";
fi
cd ~/coast/src/coastkad/
if ! python setup.py build
then
	echo "ERROR";
	return 1;
fi
if ! sudo python setup.py install
then
	echo "ERROR";
	return 1;
fi
cd
cd ~/coast/src/cdn/
if ! python setup.py build
then
	echo "ERROR";
	return 1;
fi
if ! sudo python setup.py install
then
	echo "ERROR";
	return 1;
fi
cd
cp ~/coast/utils/cdnConf.conf ~/coast/
cp ~/coast/src/cdnStart.py ~/

if ! dd if=/dev/urandom of=/home/ec2-user/coast/tmp/cdncache/cdnTempFile bs=1024 count=32
then
	echo "ERROR";
	return 1;
fi	
echo "CDN INSTALLED SUCCESFULLY!";
#return 0
