sudo yum install gcc --nogpgcheck < ~/coast/utils/yes.txt
sudo yum install sqlite --nogpgcheck < ~/coast/utils/yes.txt
sudo yum install sqlite-devel --nogpgcheck < ~/coast/utils/yes.txt
sudo yum install python-devel --nogpgcheck < ~/coast/utils/yes.txt
cd ~/coast/src/twisted/
python setup.py build
sudo python setup.py install
cd ~/coast/src/pysqlite/
python setup.py build
sudo python setup.py install
cd ~/coast/src/zope/
python setup.py build
sudo python setup.py install
cd
