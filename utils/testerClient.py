import socket
import sys

HOST, PORT = "planetlab2.cs.ucla.edu", 9999
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
	sock.connect((HOST, PORT))
	sock.send(socket.gethostname())
	received = sock.recv(1024)
finally:
	sock.close()

