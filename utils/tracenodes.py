import time, math, sys, os, subprocess, socket

def getNodes(filename):
	"""Select all dht nodes from file"""
	f = open(filename,'r')
	nodes = []
	for line in f:
		nodes.append(line.rstrip())
	f.close()
	return nodes
	
def splitLine(line):
	info = line.split()
	address = ''
	hostname = ''
	if info[1][0]=='(':
		hostname = '*'
		address = info[1].strip('()')
	else:
		hostname = info[1]
		address = info[2].strip('()')	
	return [hostname,address]
	
def splitRoute(info):
	route = info.splitlines()
	ret = []
	for line in route[1:]:
		ret.append(splitLine(line))
	if not len(ret):
		return None
	return ret
	
def traceroute(cmd, node):
	process = subprocess.Popen(cmd+' '+node, shell=True, stdout=subprocess.PIPE).stdout
	buf = process.read()
	info = ''
	while len(buf):
		info += buf
		buf = process.read()
	if not len(info):
		return None
	return info
	
def writeInfo(f, node, route, error = None):
	f.write('Destionation: '+node+'\n')
	if error:
		f.write(error)
		return True
	for info in route:
		f.write(info[0]+','+info[1]+'\n')
	f.write('-----------------------------------\n')
	return True
	

	
def mainLoop(cmd, nodes):
	f = open(socket.gethostname()+'.routes','w')
	for node in nodes:
		info = traceroute(cmd,node)
		if not info:
			error = 'ERROR: an error occurred while contacting node: '+`node`+'\n'
			writeInfo(f, node, info, error)
			continue
		info = splitRoute(info)
		if not info:
			error = 'ERROR: an error splitting information from node: '+`node`+'\n'
			writeInfo(f, node, info, error)
			continue
		res = writeInfo(f, node, info)
		if not res:
			error = 'ERROR: an error occurred while writing the information obtained from node: '+`node`+'\n'
			writeInfo(f, node, info, error)
	f.close()
	print 'Main loop finished: got the route to every node'

def startTR(cmd, filename):
	nodes = getNodes(filename)
	mainLoop(cmd, nodes)
	
if __name__ == "__main__":
	startTR(sys.argv[1], sys.argv[2])
