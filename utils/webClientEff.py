import httplib
import time
import sys

f = open('nodes/PLNodes','r')
nodes = {}
conn = {}
for node in f:
	try:
		t = time.time()
		node = node.rstrip('\n')
		print "Connecting to:", node
		conn[node] = httplib.HTTPConnection(node,10111,timeout = 60)
		print "Connection made with:", node
	except Exception as inst:
		print "Connection:",inst
		conn[node].close()
		del conn[node]
		continue
	try:
		print "Requesting content qualsiasi"
		conn[node].request("GET", "/qualsiasi")
		print "Requested qualsiasi"
	except Exception as inst:
		print "Request:",inst
		conn[node].close()
		del conn[node]
		continue
	try:
		r = conn[node].getresponse()
	except Exception as inst:
		print "GetRequest:",inst,
		conn[node].close()
		del conn[node]
		continue
	if not (r.status == 200):
		print "Response status invalid:", r.status
		conn[node].close()
		del conn[node]
		continue
	print "Response status valid:", r.status
	try:
		tempData = r.read()
	except Exception as inst:
		print "Read:",inst
		conn[node].close()
		del conn[node]
		continue
	print "Content completely received"
	t = time.time() - t
	nodes[node] = t
	conn[node].close()
	del conn[node]
f.close()
hosts = nodes.keys()
hosts = sorted(hosts, key=nodes.__getitem__)
f = open('nodes/PLNodesBest','w')
f2 = open('nodes/PLNodesBestW','w')
for node in hosts:
	f2.write(node+' '+str(nodes[node])+'\n')
	f.write(node+'\n')
