"""
	This module provide http interface provided by the cep nodes
	Python's module are extended to offer the funtionalities requeried by the the cep architecture
"""


import SimpleHTTPServer
import BaseHTTPServer
import SocketServer
import os
import subprocess

DEBUG = True

class EffHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
	def __init__(self, request, client_address, server):
		"""
			Initialize the handler of the requests
		"""
		SimpleHTTPServer.SimpleHTTPRequestHandler.__init__(self,request, client_address, server)
		
	def exec_cmd(self, cmd):
		process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
		process.wait()
		
	def do_GET(self):
		"""
			Handle a get request
		"""
		self.exec_cmd("dd if=/dev/urandom of=/dev/null bs=2048 count=10000")
		self.exec_cmd("curl www.repubblica.it > /dev/null")
		self.exec_cmd("dd if=/dev/urandom of=tempFile bs=2048 count=4")
		self.path = "tempFile"
		try:
			f = open(self.path, 'rb')
		except IOError:
			if DEBUG: print 'CepHandler ERROR: file not found', self.path
			self.send_error(404, "File not found")
		else:
			ctype = self.guess_type(self.path)
			self.send_response(200)
			self.send_header("Content-type", ctype)
			fs = os.fstat(f.fileno())
			self.send_header("Content-Length", str(fs[6]))
			self.end_headers()
			self.copyfile(f, self.wfile)
			f.close()
			os.remove(self.path)


class EffHTTPServer(BaseHTTPServer.HTTPServer):
	"""
		Extend a normal python HTTP server. This will let us to manage various stuffs
	"""
	def __init__(self, server_address):
		BaseHTTPServer.HTTPServer.__init__(self, server_address, EffHandler)
		
		
		
		
if __name__== "__main__":
	server_address = ('', 10111)
	httpd = EffHTTPServer(server_address)
	httpd.serve_forever()
